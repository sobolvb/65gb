package com.example.a65gb.repository

import android.util.Log
import com.example.a65gb.db.AppDao
import com.example.a65gb.models.Employee
import com.example.a65gb.models.Position
import com.example.a65gb.models.Response
import com.example.a65gb.network.ApiService
import java.text.SimpleDateFormat
import javax.inject.Inject


class Repository @Inject constructor(
        private val dao: AppDao,
        private val service: ApiService
) {


    suspend fun getPositions(): List<Position> {
        val positions = dao.getPositions()
        if (positions.isEmpty()) {
            val dataIsExist = downloadAndSaveData()
            if (!dataIsExist)
                return emptyList()
        } else
            return positions
        return dao.getPositions()
    }

    suspend fun getEmployees(id: Int) = dao
            .getEmployees()
            .filter { emp ->
                emp.specialty
                        .find { it.specialty_id == id }
                        ?.let { true } ?: false
            }


    suspend fun getEmployeeById(id: Int) = dao.getEmployeeById(id)


    private suspend fun downloadAndSaveData(): Boolean {
        val data: Response
        try {
            data = service.getData()
            saveDownloadedDataToDatabase(data.response)
        } catch (e: Exception) {
            return false
        }
        return true
    }

    private suspend fun saveDownloadedDataToDatabase(data: List<Employee>) {
        savePositionsToDatabase(data)
        prepareAndSaveEmployeesToDatabase(data)
    }

    private suspend fun prepareAndSaveEmployeesToDatabase(data: List<Employee>) {
        val perfectListOfEmployees = prepareEmployees(data)
        dao.insertEmployees(perfectListOfEmployees)
    }

    private fun prepareEmployees(data: List<Employee>) = data.map { oldEmployee ->
        Employee(id = oldEmployee.hashCode(),
                avatr_url = oldEmployee.avatr_url,
                birthday = if (oldEmployee.birthday.isNullOrEmpty())
                    null
                else
                    parseNiceTimeTemplateFromNiceJsonFile(oldEmployee.birthday!!),
                f_name = oldEmployee.f_name.toLowerCase().capitalize(),
                l_name = oldEmployee.l_name?.toLowerCase()?.capitalize(),
                specialty = oldEmployee.specialty)
    }


    private fun parseNiceTimeTemplateFromNiceJsonFile(birthday: String): String {
        lateinit var timeForTemplate: String
        val firstFourChar = birthday.substring(0, 4)

        timeForTemplate = if (!firstFourChar.contains('-')) {
            birthday
        } else {
            reverseWrongTime(birthday)
        }

        val date = SimpleDateFormat("yyyy-MM-dd").parse(timeForTemplate)
        return date.time.toString()
    }

    private fun reverseWrongTime(birthday: String): String {
        var list = birthday.split("-").toMutableList()
        var tmp = list[2]
        list[2] = list[0]
        list[0] = tmp
        return list.joinToString("-")
    }

    private suspend fun savePositionsToDatabase(data: List<Employee>) {
        val positions: List<Position> = data.flatMap { it.specialty }.distinct()
        dao.insertPositions(positions)
    }
}
