package com.example.a65gb.network

import com.example.a65gb.models.Response
import retrofit2.http.GET

interface ApiService {

    @GET("65gb/static/raw/master/testTask.json")
    suspend fun getData(): Response
}