package com.example.a65gb.models

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "employee_table")
data class Employee(
    @PrimaryKey(autoGenerate = false)
    var id : Int,
    val avatr_url: String?,
    var birthday: String?,
    var f_name: String,
    var l_name: String?,
    val specialty: List<Position>
)