package com.example.a65gb.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "position_table")
data class Position(
        val name: String,
        @PrimaryKey(autoGenerate = true)
        val specialty_id: Int
)