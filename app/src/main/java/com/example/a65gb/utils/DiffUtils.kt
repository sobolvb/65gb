package com.example.a65gb.utils

import androidx.recyclerview.widget.DiffUtil
import com.example.a65gb.models.Employee
import com.example.a65gb.models.Position

class DiffUtils {
    companion object {
        val positionsDifferCallback = object : DiffUtil.ItemCallback<Position>() {
            override fun areItemsTheSame(oldItem: Position, newItem:Position): Boolean {
                return oldItem.specialty_id == newItem.specialty_id
            }

            override fun areContentsTheSame(oldItem:Position, newItem: Position): Boolean {
                return oldItem == newItem
            }
        }

        val employeeDifferCallback = object : DiffUtil.ItemCallback<Employee>() {
            override fun areItemsTheSame(oldItem: Employee, newItem:Employee): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem:Employee, newItem: Employee): Boolean {
                return oldItem == newItem
            }
        }

    }
}