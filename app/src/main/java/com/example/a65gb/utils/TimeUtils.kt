package com.example.a65gb.utils

import android.util.Log
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

@Suppress("DEPRECATION")
class TimeUtils {
    companion object {
        fun dateFromTimestamp(time: String): String {
            val date = Date(time.toLong())
            val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy")
            return simpleDateFormat.format(date)
        }

        //TODO("Без учета месяцев 8( ")
        fun getAgeFromTimestamp(time: String): Int {
            val currentYear = Date().year
            val birthday  = Date(time.toLong()).year
            val age = currentYear - birthday

            return age
        }
    }
}