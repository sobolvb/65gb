package com.example.a65gb.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a65gb.R
import com.example.a65gb.adapters.EmployeeAdapter
import com.example.a65gb.databinding.FragmentEmployeesBinding
import com.example.a65gb.utils.DiffUtils
import com.example.a65gb.utils.Resource
import com.example.a65gb.viewmodel.EmployeeViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class EmployeesFragment : Fragment() {
    private val viewModel: EmployeeViewModel by viewModels()
    private lateinit var binding: FragmentEmployeesBinding
    private lateinit var employeeAdapter: EmployeeAdapter
    private val args: EmployeesFragmentArgs by navArgs()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupEmployeesList()
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_employees, container, false);
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.employees.observe(viewLifecycleOwner, {
            when (it) {
                is Resource.Loading -> showLoading()
                is Resource.Success -> {
                    employeeAdapter.submitList(it.data)
                    stopLoading()
                }
                is Resource.Failure -> {
                    Toast.makeText(requireContext(), it.throwable.toString(), Toast.LENGTH_LONG).show()
                    stopLoading()
                }
            }
        })

       viewModel.getEmployees(args.positionId)
    }

    private fun setupEmployeesList() {
        binding.rvEmployees.apply {
            employeeAdapter = EmployeeAdapter(DiffUtils.employeeDifferCallback)
            layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
            adapter = employeeAdapter
        }
        employeeAdapter.setOnItemClickListener { employee ->
            val employeeId = employee.id
            val action = EmployeesFragmentDirections.actionStaffFragmentToStaffDetailsFragment(employeeId)
            findNavController().navigate(action)
        }
    }

    private fun showLoading() {
        binding.apply {
            rvEmployees.visibility = View.GONE
            pbEmployees.visibility = View.VISIBLE
        }
    }

    private fun stopLoading() {
        binding.apply {
            pbEmployees.visibility = View.GONE
            rvEmployees.visibility = View.VISIBLE
        }
    }


}