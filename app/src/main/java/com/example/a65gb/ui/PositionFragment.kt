package com.example.a65gb.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a65gb.R
import com.example.a65gb.adapters.PositionAdapter
import com.example.a65gb.databinding.FragmentPositionsBinding
import com.example.a65gb.utils.DiffUtils
import com.example.a65gb.viewmodel.PositionViewModel
import com.example.a65gb.utils.Resource

import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PositionFragment : Fragment() {

    private val viewModel: PositionViewModel by viewModels()
    private lateinit var binding: FragmentPositionsBinding
    private lateinit var positionAdapter: PositionAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupPositionsList()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_positions, container, false);
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.positions.observe(viewLifecycleOwner, {
            when (it) {
                is Resource.Loading -> showLoading()
                is Resource.Success -> {
                    positionAdapter.submitList(it.data)
                    stopLoading()
                }
                is Resource.Failure -> {
                    Toast.makeText(requireContext(), it.throwable.toString(), Toast.LENGTH_LONG).show()
                    stopLoading()
                }
            }
        })
    }

    private fun setupPositionsList() {
        binding.rvPositions.apply {
            positionAdapter = PositionAdapter(DiffUtils.positionsDifferCallback)
            layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
            adapter = positionAdapter
        }
        positionAdapter.setOnItemClickListener { position ->
            val positionId = position.specialty_id
            val action = PositionFragmentDirections.actionSpecialtyFragmentToStaffFragment(positionId)
            findNavController().navigate(action)
        }
    }

    private fun showLoading() {
        binding.apply {
            rvPositions.visibility = View.GONE
            pbPositions.visibility = View.VISIBLE
        }
    }

    private fun stopLoading() {
        binding.apply {
            pbPositions.visibility = View.GONE
            rvPositions.visibility = View.VISIBLE
        }
    }

}