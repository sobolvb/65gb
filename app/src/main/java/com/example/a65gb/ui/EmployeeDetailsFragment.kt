package com.example.a65gb.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.a65gb.R
import com.example.a65gb.databinding.FragmentEmployeeDetailsBinding
import com.example.a65gb.models.Employee
import com.example.a65gb.utils.Resource
import com.example.a65gb.utils.TimeUtils
import com.example.a65gb.viewmodel.EmployeeDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class EmployeeDetailsFragment : Fragment() {

    private val viewModel: EmployeeDetailsViewModel by viewModels()
    private lateinit var binding: FragmentEmployeeDetailsBinding
    private val args: EmployeeDetailsFragmentArgs by navArgs()


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_employee_details, container, false);
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.employee.observe(viewLifecycleOwner, {
            when (it) {
                is Resource.Loading -> showLoading()
                is Resource.Success -> {
                    it.data?.let { employeeInfo -> showData(employeeInfo) }
                    stopLoading()
                }
                is Resource.Failure -> {
                    Toast.makeText(requireContext(), it.throwable.toString(), Toast.LENGTH_LONG).show()
                    stopLoading()
                }
            }
        })
        viewModel.getEmployeeDetails(args.id)
    }

    private fun showData(data: Employee) {
        binding.apply {
            tvAge.text = data.birthday?.let { TimeUtils.getAgeFromTimestamp(it).toString() + " лет" } ?: "-"
            tvFName.text = data.f_name
            tvLName.text = data.l_name
            tvBirthday.text = data.birthday?.let { TimeUtils.dateFromTimestamp(it)  + " г." }
                    ?: "-"
            tvPosition.text = data.specialty.map { it.name }.toString().removePrefix("[").removeSuffix("]")
        }
    }

    private fun showLoading() {
        binding.apply {
            cvEmployeeDetails.visibility = View.GONE
            pbEmployeeDetails.visibility = View.VISIBLE
        }
    }

    private fun stopLoading() {
        binding.apply {
            pbEmployeeDetails.visibility = View.GONE
            cvEmployeeDetails.visibility = View.VISIBLE
        }
    }
}