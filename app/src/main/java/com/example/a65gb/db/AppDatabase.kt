package com.example.a65gb.db

import androidx.room.Database

import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.a65gb.models.Employee
import com.example.a65gb.models.Position


@Database(entities = [Position::class, Employee::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun employeesDao(): AppDao
}