package com.example.a65gb.db

import androidx.room.*
import com.example.a65gb.models.Employee
import com.example.a65gb.models.Position

@Dao
interface AppDao {

    //Positions
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPositions(positions: List<Position>)

    @Query("SELECT * FROM position_table")
    suspend fun getPositions(): List<Position>

    //Employees
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertEmployees(positions: List<Employee>)

    @Query("SELECT * FROM employee_table")
    suspend fun getEmployees(): List<Employee>

    //Employee details
    @Query("SELECT * FROM employee_table WHERE :id == id")
    suspend fun getEmployeeById(id : Int): Employee

}