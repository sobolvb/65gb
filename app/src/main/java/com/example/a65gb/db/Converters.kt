package com.example.a65gb.db

import androidx.room.TypeConverter
import com.example.a65gb.models.Position
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class Converters {

    var gson = Gson()
    var type = object : TypeToken<List<Position>>() {
    }.type

    @TypeConverter
    fun stringToNestedDatae(json: String?): List<Position> {
        return gson.fromJson(json, type)
    }

    @TypeConverter
    fun nestedDataToStringe(nestedData: List<Position>): String {
        return gson.toJson(nestedData, type)
    }
}
