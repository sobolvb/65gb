package com.example.a65gb.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a65gb.models.Position
import com.example.a65gb.repository.Repository
import com.example.a65gb.utils.Resource

import kotlinx.coroutines.launch
import java.lang.Exception

class PositionViewModel @ViewModelInject constructor(
    private val mainRepository: Repository
) : ViewModel() {

    private val _positions = MutableLiveData<Resource<List<Position>>>()

    val positions: LiveData<Resource<List<Position>>>
        get() = _positions

    init {
        getPositions()
    }

    private fun getPositions() = viewModelScope.launch {
        _positions.postValue(Resource.Loading())
        try {
            val data = mainRepository.getPositions()
            if (data.isEmpty())
                _positions.postValue(Resource.Failure(Throwable("List wasn't downloaded. Nothing to show.")))
            else
                _positions.postValue(Resource.Success(data))
        } catch (e: Exception) {
            _positions.postValue(Resource.Failure(e))
        }

    }
}