package com.example.a65gb.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a65gb.models.Employee
import com.example.a65gb.models.Position
import com.example.a65gb.repository.Repository
import com.example.a65gb.utils.Resource
import kotlinx.coroutines.launch
import java.lang.Exception

class EmployeeViewModel @ViewModelInject constructor(
        private val mainRepository: Repository
) : ViewModel() {

    private val _employees = MutableLiveData<Resource<List<Employee>>>()

    val employees: LiveData<Resource<List<Employee>>>
        get() = _employees

    fun getEmployees(id: Int) = viewModelScope.launch {
        _employees.postValue(Resource.Loading())
        try {
            val data = mainRepository.getEmployees(id)
            _employees.postValue(Resource.Success(data))
        } catch (e: Exception) {
            _employees.postValue(Resource.Failure(e))
        }

    }
}