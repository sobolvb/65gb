package com.example.a65gb.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a65gb.models.Employee
import com.example.a65gb.repository.Repository
import com.example.a65gb.utils.Resource
import kotlinx.coroutines.launch
import java.lang.Exception

class EmployeeDetailsViewModel @ViewModelInject constructor(
        private val mainRepository: Repository
) : ViewModel() {

    private val _employee = MutableLiveData<Resource<Employee>>()

    val employee: LiveData<Resource<Employee>>
        get() = _employee


    fun getEmployeeDetails(id: Int)= viewModelScope.launch {
        _employee.postValue(Resource.Loading())
        try {
            val data = mainRepository.getEmployeeById(id)
            _employee.postValue(Resource.Success(data))
        } catch (e: Exception) {
            _employee.postValue(Resource.Failure(e))
        }

    }
}