package com.example.a65gb.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.a65gb.R
import com.example.a65gb.models.Employee
import com.example.a65gb.models.Position
import com.example.a65gb.utils.TimeUtils
import com.google.android.material.card.MaterialCardView

class EmployeeAdapter(diffCallback: DiffUtil.ItemCallback<Employee>) :
        ListAdapter<Employee, EmployeeAdapter.EmployeeViewHolder>(diffCallback) {

    private var onItemClickListener: ((Employee) -> Unit)? = null

    inner class EmployeeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeViewHolder {
        val itemView =
                LayoutInflater.from(parent.context).inflate(R.layout.item_employee, parent, false)
        return EmployeeViewHolder(itemView)

    }

    override fun onBindViewHolder(holder: EmployeeViewHolder, position: Int) {

        val fName = holder.itemView.findViewById<TextView>(R.id.tv_fName)
        val lName = holder.itemView.findViewById<TextView>(R.id.tv_lName)
        val birhday = holder.itemView.findViewById<TextView>(R.id.tv_birthday)
        val card = holder.itemView.findViewById<MaterialCardView>(R.id.cv_employee)

        getItem(position)?.let { item ->
            fName.text = item.f_name
            lName.text = item.l_name
            birhday.text = item.birthday?.let { "Возраст: " + TimeUtils.getAgeFromTimestamp(it).toString() }
                    ?: "-"

            card.setOnClickListener {
                onItemClickListener?.let { it(item) }
            }
        }

    }

    fun setOnItemClickListener(listener: (Employee) -> Unit) {
        onItemClickListener = listener
    }


}