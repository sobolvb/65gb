package com.example.a65gb.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.example.a65gb.R
import com.example.a65gb.models.Position
import com.google.android.material.card.MaterialCardView

class PositionAdapter(diffCallback: DiffUtil.ItemCallback<Position>) :
    ListAdapter<Position, PositionAdapter.PositionViewHolder>(diffCallback) {


    private var onItemClickListener: ((Position) -> Unit)? = null

    inner class PositionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PositionViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_position, parent, false)
        return PositionViewHolder(itemView)

    }

    override fun onBindViewHolder(holder: PositionViewHolder, position: Int) {

        val positionName = holder.itemView.findViewById<TextView>(R.id.tv_position_name)
        val card = holder.itemView.findViewById<MaterialCardView>(R.id.cv_position)

        getItem(position)?.let { item ->
            positionName.text = item.name
            card.setOnClickListener {
                    onItemClickListener?.let { it(item) }
                }
            }

    }

    fun setOnItemClickListener(listener: (Position) -> Unit) {
        onItemClickListener = listener
    }


}